//
//  FullDetailsModel.swift
//  Imdb
//
//  Created by Zakhar on 12/24/20.
//

import Foundation

struct FullDetailsOfFilm: Codable {
    var Title: String?
    var Released: String?
    var Genre: String?
    var imdbRating: String?
    var Poster: String?
    var Writer: String?
    var Director: String?
    var Actors: String?
    var Country: String?
    var Language: String?
}
