//
//  FilmListViewController.swift
//  Imdb
//
//  Created by Zakhar Sidorov  on 12/16/20.
//

import UIKit

class TableViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    
    
    private  let dataProvider = DataProviderTableView()
    
    private let delegateTableView = DelegateTableView()
    
//    var manager: Manager = NetworkManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = dataProvider
        tableView.delegate = delegateTableView
        delegateTableView.controller = self
        tableView.rowHeight = 200
        request()
        
    }
    
    
    
    func request()  {
        NetworkManager.genericFetch(urlString: API.movieHacker.rawValue, complition: { (films: Result) in
            print(films)
            if let films = films.Search {
                self.dataProvider.films = films
                self.delegateTableView.films = films
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
        
        
    }
    
}
