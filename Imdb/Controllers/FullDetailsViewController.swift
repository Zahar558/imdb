//
//  FullDetailsViewController.swift
//  Imdb
//
//  Created by Zakhar Sidorov  on 12/27/20.
//

import UIKit

class FullDetailsViewController: UIViewController {
    
    let detailsView =  FullDetailsView()
    
    
    private var detailsModel = FullDetailsOfFilm()
    var imdbID: String = ""
    private var url: String  {
        get {
            return "https://www.omdbapi.com/?apikey=9cee03af&i=\(imdbID)"
        }
    }
    private var api: String {
        get {
            API.selectedMovie.rawValue + imdbID
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLabels()
        
    }
    
    override func loadView() {
        view = detailsView
        detailsView.constraintsSetUp()
        
    }
    
    private func setUpLabels()  {
        NetworkManager.genericFetch(urlString: api) { (film: FullDetailsOfFilm) in
            self.detailsModel = film
            guard let title = self.detailsModel.Title,
                  let released = self.detailsModel.Released,
                  let genre = self.detailsModel.Genre,
                  let rating = self.detailsModel.imdbRating,
                  let poster = URL(string: self.detailsModel.Poster ?? ""),
                  let writer = self.detailsModel.Writer,
                  let director = self.detailsModel.Director,
                  let actors = self.detailsModel.Actors,
                  let country = self.detailsModel.Country,
                  let language = self.detailsModel.Language
            else { return }
            
            DispatchQueue.main.async {
                self.detailsView.titleInfoTextLabel.text = title
                self.detailsView.releasedInfoTextLabel.text = released
                self.detailsView.genreInfoTextLabel.text = genre
                self.detailsView.imdbRatingInfoTextLabel.text = rating
                self.detailsView.writerInfoTextLabel.text = writer
                self.detailsView.directorInfoTextLabel.text = director
                self.detailsView.actorsInfoTextLabel.text = actors
                self.detailsView.countryInfoTextLabel.text = country
                self.detailsView.languageInfoTextLabel.text = language
                
                ImageLoader.shared.imageFromURL(poster) { (image) in
                    DispatchQueue.main.async {
                        self.detailsView.posterImageView.image = image
                        
                    }
                }
            }
            
        }
    }
}


