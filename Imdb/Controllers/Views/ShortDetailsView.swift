//
//  ShortViewController.swift
//  Imdb
//
//  Created by Zakhar Sidorov  on 12/28/20.
//

import UIKit

class ShortDetailsView: UIView {
    
    
    let posterImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .black
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Title:"
        return label
    }()
    
    let titleInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 19)
        return label
    }()
    
    let releasedLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Released:"
        return label
    }()
    
    let releasedInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        label.textColor = .white
        return label
    }()
    
    
    let genreLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Genre:"
        return label
    }()
    let genreInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        label.textColor = .white
        return label
    }()
    
    let imbdRatingLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Rating:"
        return label
    }()
    
    let imdbRatingInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        label.textColor = .white
        return label
    }()
    
    func setUpViews() {
        addSubview(titleLabel)
        addSubview(titleInfoTextLabel)
        addSubview(posterImageView)
        addSubview(releasedLabel)
        addSubview(releasedInfoTextLabel)
        addSubview(genreLabel)
        addSubview(genreInfoTextLabel)
        addSubview(imbdRatingLabel)
        addSubview(imdbRatingInfoTextLabel)
    
    }
    
    
    
    func constraintsSetUp() {
        
        
        NSLayoutConstraint.activate([
            posterImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            posterImageView.topAnchor.constraint(equalTo: topAnchor),
            posterImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            posterImageView.heightAnchor.constraint(equalToConstant: 300),
            posterImageView.widthAnchor.constraint(equalTo: widthAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: posterImageView.bottomAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: titleInfoTextLabel.leadingAnchor),
            titleLabel.widthAnchor.constraint(equalToConstant: 100),
            titleLabel.heightAnchor.constraint(equalToConstant: 40),
            ///!!!
            titleInfoTextLabel.topAnchor.constraint(equalTo: posterImageView.bottomAnchor),
            titleInfoTextLabel.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            titleInfoTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            titleInfoTextLabel.heightAnchor.constraint(equalToConstant: 40),
            ///!!!!
            releasedLabel.topAnchor.constraint(equalTo: titleInfoTextLabel.bottomAnchor, constant: 30),
            releasedLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            releasedLabel.trailingAnchor.constraint(equalTo: releasedInfoTextLabel.leadingAnchor),
            releasedLabel.widthAnchor.constraint(equalToConstant: 100),
            releasedLabel.heightAnchor.constraint(equalToConstant: 40),
            
            releasedInfoTextLabel.topAnchor.constraint(equalTo: titleInfoTextLabel.bottomAnchor, constant: 30),
            releasedInfoTextLabel.leadingAnchor.constraint(equalTo: releasedLabel.trailingAnchor),
            releasedInfoTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -50),
            releasedInfoTextLabel.heightAnchor.constraint(equalToConstant: 40),
            
            genreLabel.topAnchor.constraint(equalTo: releasedInfoTextLabel.bottomAnchor, constant: 30),
            genreLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            genreLabel.trailingAnchor.constraint(equalTo: genreInfoTextLabel.leadingAnchor),
            genreLabel.widthAnchor.constraint(equalToConstant: 100),
            genreLabel.heightAnchor.constraint(equalToConstant: 40),
            
            genreInfoTextLabel.topAnchor.constraint(equalTo: releasedInfoTextLabel.bottomAnchor, constant: 30),
            genreInfoTextLabel.leadingAnchor.constraint(equalTo: genreLabel.trailingAnchor),
            genreInfoTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            genreInfoTextLabel.heightAnchor.constraint(equalToConstant: 40),
            
            imbdRatingLabel.topAnchor.constraint(equalTo: genreInfoTextLabel.bottomAnchor, constant: 30),
            imbdRatingLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            imbdRatingLabel.trailingAnchor.constraint(equalTo: imdbRatingInfoTextLabel.leadingAnchor),
            imbdRatingLabel.widthAnchor.constraint(equalToConstant: 100),
            imbdRatingLabel.heightAnchor.constraint(equalToConstant: 40),
            
            imdbRatingInfoTextLabel.topAnchor.constraint(equalTo: genreInfoTextLabel.bottomAnchor, constant: 30),
            imdbRatingInfoTextLabel.leadingAnchor.constraint(equalTo: imbdRatingLabel.trailingAnchor),
            imdbRatingInfoTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            imdbRatingInfoTextLabel.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    func landscapeConstraints() {
        
        
        NSLayoutConstraint.activate([
            posterImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            posterImageView.topAnchor.constraint(equalTo: topAnchor),
            posterImageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            posterImageView.widthAnchor.constraint(equalTo: posterImageView.heightAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: posterImageView.trailingAnchor, constant: 10),
            titleLabel.heightAnchor.constraint(equalToConstant: 65),
            titleLabel.widthAnchor.constraint(equalToConstant: 100),
            ///!!!
            titleInfoTextLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            titleInfoTextLabel.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            titleInfoTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            titleInfoTextLabel.heightAnchor.constraint(equalToConstant: 65),
            ///!!!!
            releasedLabel.topAnchor.constraint(equalTo: titleInfoTextLabel.bottomAnchor, constant: 10),
            releasedLabel.leadingAnchor.constraint(equalTo: posterImageView.trailingAnchor, constant: 10),
            releasedLabel.trailingAnchor.constraint(equalTo: releasedInfoTextLabel.leadingAnchor),
            releasedLabel.widthAnchor.constraint(equalToConstant: 100),
            releasedLabel.heightAnchor.constraint(equalToConstant: 65),
            
            releasedInfoTextLabel.topAnchor.constraint(equalTo: titleInfoTextLabel.bottomAnchor, constant: 30),
            releasedInfoTextLabel.leadingAnchor.constraint(equalTo: releasedLabel.trailingAnchor),
            releasedInfoTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            releasedInfoTextLabel.heightAnchor.constraint(equalToConstant: 65),
            
            genreLabel.topAnchor.constraint(equalTo: releasedLabel.bottomAnchor, constant: 10),
            genreLabel.leadingAnchor.constraint(equalTo: posterImageView.trailingAnchor, constant: 10),
            genreLabel.trailingAnchor.constraint(equalTo: genreInfoTextLabel.leadingAnchor),
            genreLabel.widthAnchor.constraint(equalToConstant: 100),
            genreLabel.heightAnchor.constraint(equalToConstant: 65),
            
            genreInfoTextLabel.topAnchor.constraint(equalTo: releasedInfoTextLabel.bottomAnchor, constant: 10),
            genreInfoTextLabel.leadingAnchor.constraint(equalTo: genreLabel.trailingAnchor),
            genreInfoTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            genreInfoTextLabel.heightAnchor.constraint(equalToConstant: 65),
            
            imbdRatingLabel.topAnchor.constraint(equalTo: genreLabel.bottomAnchor, constant: 10),
            imbdRatingLabel.leadingAnchor.constraint(equalTo: posterImageView.trailingAnchor, constant: 10),
            imbdRatingLabel.widthAnchor.constraint(equalToConstant: 100),
            imbdRatingLabel.heightAnchor.constraint(equalToConstant: 65),
            
            imdbRatingInfoTextLabel.topAnchor.constraint(equalTo: genreInfoTextLabel.bottomAnchor, constant: 10),
            imdbRatingInfoTextLabel.leadingAnchor.constraint(equalTo: imbdRatingLabel.trailingAnchor),
            imdbRatingInfoTextLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            imdbRatingInfoTextLabel.heightAnchor.constraint(equalToConstant: 65),
        ])
        
    }
    
  
    
}
