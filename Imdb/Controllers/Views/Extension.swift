//
//  Extension.swift
//  Imdb
//
//  Created by Zakhar Sidorov  on 12/29/20.
//

import Foundation
import  UIKit

extension UIView {
    func clearConstraints() {
        for subview in self.subviews {
            subview.clearConstraints()
        }
        self.removeConstraints(self.constraints)
    }
}
