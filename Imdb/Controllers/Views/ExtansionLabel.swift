//
//  EX_Label.swift
//  Imdb
//
//  Created by Zakhar Sidorov  on 12/28/20.
//

import Foundation
import UIKit

extension UILabel {
    func setOrangeBackground() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .orange
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 19)
        return label
        
        
    }
    func setForInfoLabels() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = .white
        
        return label 
    }
    
}

