//
//  FullDetailsViewController.swift
//  Imdb
//
//  Created by Zakhar on 12/24/20.
//

import UIKit

class FullDetailsView: UIView {
    
    //  var myScrollView = UIScrollView()
    
    let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.backgroundColor = .black
        return scroll
    }()
    
    let posterImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .black
        return image
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Title:"
        
        return label
    }()
    
    let titleInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        label.font = UIFont.boldSystemFont(ofSize: 19)
        return label
    }()
    
    
    let releasedLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Released:"
        
        return label
    }()
    
    let releasedInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        return label
    }()
    
    
    let genreLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Genre:"
        return label
    }()
    let genreInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        return label
    }()
    
    let imbdRatingLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Rating:"
        return label
    }()
    let imdbRatingInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        return label
    }()
    
    let writerLabel : UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Writer:"
        return label
    }()
    let writerInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        return label
    }()
    
    let directorLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Director:"
        return label
    }()
    let directorInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        return label
    }()
    
    let actorsLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Actors:"
        return label
    }()
    
    let actorsInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        //  label.text = "Genreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
        return label
    }()
    
    let countryLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Country:"
        return label
    }()
    
    let countryInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        //   label.text = "Genreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
        return label
    }()
    
    let languageLabel: UILabel = {
        let label = UILabel().setOrangeBackground()
        label.text = "Language:"
        return label
    }()
    
    let languageInfoTextLabel: UILabel = {
        let label = UILabel().setForInfoLabels()
        //label.text = "Genreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
        return label
    }()
    
 
    
    
    func constraintsSetUp() {
        addSubview(scrollView)
        
        scrollView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: topAnchor, constant: 36.0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        
        
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(titleInfoTextLabel)
        
        scrollView.addSubview(posterImageView)
        scrollView.addSubview(releasedLabel)
        scrollView.addSubview(releasedInfoTextLabel)
        scrollView.addSubview(genreLabel)
        scrollView.addSubview(genreInfoTextLabel)
        scrollView.addSubview(imbdRatingLabel)
        scrollView.addSubview(imdbRatingInfoTextLabel)
        scrollView.addSubview(writerLabel)
        scrollView.addSubview(writerInfoTextLabel)
        scrollView.addSubview(directorLabel)
        scrollView.addSubview(directorInfoTextLabel)
        scrollView.addSubview(actorsLabel)
        scrollView.addSubview(actorsInfoTextLabel)
        scrollView.addSubview(countryLabel)
        scrollView.addSubview(countryInfoTextLabel)
        scrollView.addSubview(languageLabel)
        scrollView.addSubview(languageInfoTextLabel)
        
        
        
        NSLayoutConstraint.activate([
            posterImageView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            posterImageView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            posterImageView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            posterImageView.heightAnchor.constraint(equalToConstant: 350),
            posterImageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: posterImageView.bottomAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: titleInfoTextLabel.leadingAnchor),
            titleLabel.widthAnchor.constraint(equalToConstant: 100),
            ///!!!
            titleInfoTextLabel.topAnchor.constraint(equalTo: posterImageView.bottomAnchor),
            titleInfoTextLabel.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            titleInfoTextLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            ///!!!!
            releasedLabel.topAnchor.constraint(equalTo: titleInfoTextLabel.bottomAnchor, constant: 30),
            releasedLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            releasedLabel.trailingAnchor.constraint(equalTo: releasedInfoTextLabel.leadingAnchor),
            releasedLabel.widthAnchor.constraint(equalToConstant: 100),
            
            releasedInfoTextLabel.topAnchor.constraint(equalTo: titleInfoTextLabel.bottomAnchor, constant: 30),
            releasedInfoTextLabel.leadingAnchor.constraint(equalTo: releasedLabel.trailingAnchor),
            releasedInfoTextLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -50),
            
            genreLabel.topAnchor.constraint(equalTo: releasedInfoTextLabel.bottomAnchor, constant: 30),
            genreLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            genreLabel.trailingAnchor.constraint(equalTo: genreInfoTextLabel.leadingAnchor),
            genreLabel.widthAnchor.constraint(equalToConstant: 100),
            
            
            genreInfoTextLabel.topAnchor.constraint(equalTo: releasedInfoTextLabel.bottomAnchor, constant: 30),
            genreInfoTextLabel.leadingAnchor.constraint(equalTo: genreLabel.trailingAnchor),
            genreInfoTextLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            
            imbdRatingLabel.topAnchor.constraint(equalTo: genreInfoTextLabel.bottomAnchor, constant: 30),
            imbdRatingLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            imbdRatingLabel.trailingAnchor.constraint(equalTo: imdbRatingInfoTextLabel.leadingAnchor),
            imbdRatingLabel.widthAnchor.constraint(equalToConstant: 100),
            
            imdbRatingInfoTextLabel.topAnchor.constraint(equalTo: genreInfoTextLabel.bottomAnchor, constant: 30),
            imdbRatingInfoTextLabel.leadingAnchor.constraint(equalTo: imbdRatingLabel.trailingAnchor),
            imdbRatingInfoTextLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            
            
            writerLabel.topAnchor.constraint(equalTo: imdbRatingInfoTextLabel.bottomAnchor, constant: 30),
            writerLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            writerLabel.trailingAnchor.constraint(equalTo: writerInfoTextLabel.leadingAnchor),
            writerLabel.widthAnchor.constraint(equalToConstant: 100),
            
            writerInfoTextLabel.topAnchor.constraint(equalTo: imdbRatingInfoTextLabel.bottomAnchor, constant: 30),
            writerInfoTextLabel.leadingAnchor.constraint(equalTo: writerLabel.trailingAnchor),
            writerInfoTextLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            
            
            
            directorLabel.topAnchor.constraint(equalTo: writerInfoTextLabel.bottomAnchor,constant: 30),
            directorLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            directorLabel.trailingAnchor.constraint(equalTo: directorInfoTextLabel.leadingAnchor),
            directorLabel.widthAnchor.constraint(equalToConstant: 100),
            
            
            directorInfoTextLabel.topAnchor.constraint(equalTo: writerInfoTextLabel.bottomAnchor, constant: 30),
            directorInfoTextLabel.leadingAnchor.constraint(equalTo: writerLabel.trailingAnchor),
            directorInfoTextLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            
            actorsLabel.topAnchor.constraint(equalTo: directorInfoTextLabel.bottomAnchor,constant: 30),
            actorsLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            actorsLabel.trailingAnchor.constraint(equalTo: actorsInfoTextLabel.leadingAnchor),
            actorsLabel.widthAnchor.constraint(equalToConstant:100),
            
            actorsInfoTextLabel.topAnchor.constraint(equalTo: directorInfoTextLabel.bottomAnchor, constant: 30),
            actorsInfoTextLabel.leadingAnchor.constraint(equalTo: actorsLabel.trailingAnchor),
            actorsInfoTextLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            
            
            countryLabel.topAnchor.constraint(equalTo: actorsInfoTextLabel.bottomAnchor, constant: 20),
            countryLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            countryLabel.trailingAnchor.constraint(equalTo: countryInfoTextLabel.leadingAnchor),
            countryLabel.widthAnchor.constraint(equalToConstant: 100),
            
            countryInfoTextLabel.topAnchor.constraint(equalTo: actorsInfoTextLabel.bottomAnchor, constant: 20),
            countryInfoTextLabel.leadingAnchor.constraint(equalTo: writerLabel.trailingAnchor),
            countryInfoTextLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            
            languageLabel.topAnchor.constraint(equalTo: countryInfoTextLabel.bottomAnchor, constant: 20),
            languageLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            languageLabel.trailingAnchor.constraint(equalTo: languageInfoTextLabel.leadingAnchor),
            languageLabel.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20),
            languageLabel.widthAnchor.constraint(equalToConstant: 100),
            
            languageInfoTextLabel.topAnchor.constraint(equalTo: countryInfoTextLabel.bottomAnchor, constant: 30),
            languageInfoTextLabel.leadingAnchor.constraint(equalTo: languageLabel.trailingAnchor),
            languageInfoTextLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            languageInfoTextLabel.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20)
            
            
        ])
    }
}


