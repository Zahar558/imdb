//
//  DetailsViewController.swift
//  Imdb
//
//  Created by Zakhar Sidorov  on 12/20/20.
//

import UIKit

class ShortDetailsViewController: UIViewController {
    
    
    let shortDetailsView = ShortDetailsView()
    
    private var detailsModel = ShortDetailsOfFilm()
    var imdbID: String = ""
    private var url: String  {
        get {
            return "https://www.omdbapi.com/?apikey=9cee03af&i=\(imdbID)"
        }
    }
    private var api: String {
        get {
            API.selectedMovie.rawValue + imdbID
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLabels()
        
        
    }
    override func loadView() {
        view = shortDetailsView
        shortDetailsView.setUpViews()
        shortDetailsView.constraintsSetUp()
        
        
        
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            shortDetailsView.clearConstraints()
            shortDetailsView.landscapeConstraints()
        } else if  UIDevice.current.orientation.isPortrait {
            shortDetailsView.clearConstraints()
            shortDetailsView.constraintsSetUp()
        }
    }
    
    
    //    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
    //        if(toInterfaceOrientation.isLandscape) {
    //            shortDetailsView.clearConstraints()
    //            shortDetailsView.landscapeConstraints()
    //        } else if (toInterfaceOrientation.isPortrait) {
    //            shortDetailsView.clearConstraints()
    //            shortDetailsView.constraintsSetUp()
    //        } else if(toInterfaceOrientation.isLandscape.)
    //    }
    
    
    private func setUpLabels() {
        NetworkManager.genericFetch(urlString: api ) { (film: ShortDetailsOfFilm) in
            self.detailsModel = film
            DispatchQueue.main.async {
                self.shortDetailsView.imdbRatingInfoTextLabel.text = self.imdbID
                if let title = self.detailsModel.Title {
                    self.shortDetailsView.titleInfoTextLabel.text = title
                }
                if let genre = self.detailsModel.Genre {
                    self.shortDetailsView.genreInfoTextLabel.text = genre
                }
                if let rating = self.detailsModel.imdbRating {
                    self.shortDetailsView.imdbRatingInfoTextLabel.text = rating
                }
                if let released = self.detailsModel.Released {
                    self.shortDetailsView.releasedInfoTextLabel.text = released
                }
                if let url = URL(string: self.detailsModel.Poster ?? "" ) {
                    ImageLoader.shared.imageFromURL(url) { (image) in
                        DispatchQueue.main.async {
                            self.shortDetailsView.posterImageView.image = image
                        }
                    }
                    
                }
                
            }
        }
        
    }
}



